
## License

This repository is updated automatically by
[debdistreproduce](https://gitlab.com/debdistutils/debdistreproduce)
but to the extent anything is copyrightable, everything is published
under the AGPLv3+ see the file [COPYING](COPYING) or
[https://www.gnu.org/licenses/agpl-3.0.en.html].

## Contact

The maintainer of this project is [Simon
Josefsson](https://blog.josefsson.org/).
