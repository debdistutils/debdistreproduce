#!/bin/sh

# Copyright (C) 2023 Simon Josefsson <simon@josefsson.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

LANG=C
export LANG
LC_ALL=C
export LC_ALL

cat lists/needs-build.txt | while read PKGVER; do
    test -z "$PKGVER" && exit 0

    PKG=$(echo $PKGVER | sed -n 's,=.*,,p')
    VER=$(echo $PKGVER | sed -n 's,.*=,,p')

    test -f logs/$PKG/$VER/build-started.txt && continue
    test -f logs/$PKG/$VER/buildlog.txt && continue
    grep -q "^$pkg=$ver\$" lists/disabled-downstream.txt 2>/dev/null && continue

    echo $PKGVER
done > tmp

mv tmp lists/needs-build.txt

exit 0
