# GitLab CI/CD Runner with Podman on Trisquel GNU/Linux 11 aramo

Install [Trisquel GNU/Linux 11 aramo](https://trisquel.info) on a
machine so it has Internet connectivity and that you get a root promt,
either through SSH or on the machine.  I don't recommend using this
machine for anything else -- the gitlab-runner and podman external
package repositories may be evil -- so use a throw-away virtual
machine.

Follow [gitlab-runner's official installation
instructions](https://docs.gitlab.com/runner/install/linux-repository.html)
documentation, but make a small variation to simulate it being run on
Ubuntu 22.04 jammy:

```
wget -q "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh"
os=ubuntu dist=jammy bash ./script.deb.sh
```

For some reason the script ends up putting the GnuPG key properly in
`/etc/apt/trusted.gpg.d` but has the wrong path in
`/etc/apt/sources.list.d/runner_gitlab-runner.list`, so fix it:

```
sed -i s,/usr/share/keyrings/runner_gitlab-runner-archive-keyring.gpg,/etc/apt/trusted.gpg.d/runner_gitlab-runner.gpg, /etc/apt/sources.list.d/runner_gitlab-runner.list
```

Next install gitlab-runner and configure it.  Replace registration
token `REPLACEME` with what is found for admins on
[https://gitlab.com/debdistutils/reproduce/trisquel/-/settings/ci_cd]
(expanding `Runners`), or higher up in the project hierarchy, such as
[https://gitlab.com/groups/debdistutils/-/runners] clicking on `...`
to show the registration token.

```
apt-get update
apt-get install gitlab-runner
gitlab-runner register --non-interactive --url https://gitlab.com/ --name glr1.sjd.se --executor docker --docker-image kpengboy/trisquel:11.0 --tag-list glr1 --registration-token REPLACEME
```

The `podman` in Trisquel is too old, so we set about installing
external packages for this, following [official podman Ubuntu
installation](https://podman.io/getting-started/installation.html)
however modifying the call to `$(lsb_release -rs)` with `22.04` below
to make it work.

```
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.opensuse.org/repositories/devel:kubic:libcontainers:unstable/xUbuntu_22.04/Release.key \
  | gpg --dearmor \
  | sudo tee /etc/apt/keyrings/devel_kubic_libcontainers_unstable.gpg > /dev/null
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/devel_kubic_libcontainers_unstable.gpg]\
    https://download.opensuse.org/repositories/devel:kubic:libcontainers:unstable/xUbuntu_22.04/ /" \
  | sudo tee /etc/apt/sources.list.d/devel:kubic:libcontainers:unstable.list > /dev/null
sudo apt-get update -qq
sudo apt-get -qq -y install podman
```

Finally we need to [configure gitlab-runner to use
podman](https://docs.gitlab.com/runner/executors/docker.html#use-podman-to-run-docker-commands),
and to use non-root user you need to login as `gitlab-runner` and make
some modifications, here is how I setup ssh to do this.

```
# gitlab-runner stop
# usermod --add-subuids 100000-165535 --add-subgids 100000-165535 gitlab-runner
# cp -a /root/.ssh/ /home/gitlab-runner/
# chown -R gitlab-runner.gitlab-runner /home/gitlab-runner/.ssh/
$ ssh gitlab-runner@glr1
$ systemctl --user --now enable podman.socket
$ systemctl --user --now start podman.socket
$ su -c 'loginctl enable-linger gitlab-runner'
```

We modify `/etc/gitlab-runner/config.toml` as follows.  See [feature
flags
documentation](https://docs.gitlab.com/runner/configuration/feature-flags.html)
for more documentation.

```
concurrent = 6

[[runners]]
    environment = ["FF_NETWORK_PER_BUILD=1", "FF_USE_FASTZIP=1"]
    [runners.docker]
        host = "unix:///run/user/999/podman/podman.sock"
```

Restart the system, and gitlab-runner should now be receiving jobs and
running them in podman.

Happy Hacking!
