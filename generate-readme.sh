#!/bin/sh

# Copyright (C) 2023 Simon Josefsson <simon@josefsson.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

LANG=C
export LANG
LC_ALL=C
export LC_ALL
LC_TIME=C
export LC_TIME
TZ=UTC0
export TZ

PROJECTURL="$1"
PAGESURL="$2"
MAPPINGS="$3"

TOTAL=$(cat lists/unique-downstream.txt 2>/dev/null | wc -l)
UNIQUEPKGS=$(cat lists/unique-downstream.txt 2>/dev/null | sed 's,=.*,,' | sort | uniq | wc -l)
LOGGED=$(find logs/ -name buildlog.txt 2>/dev/null | wc -l)
FTBFS=$(find logs/ -name ftbfs.txt 2>/dev/null | wc -l)
REPRODUCIBLE=$(find logs/ -name reproducible.txt 2>/dev/null | wc -l)
UNREPRODUCIBLE=$(expr $LOGGED - $FTBFS - $REPRODUCIBLE || true)
DISABLED=$(grep -v -e '^#' -e '^\s*$' lists/disabled-downstream.txt 2>/dev/null | wc -l)
REST=$(expr $TOTAL - $REPRODUCIBLE - $UNREPRODUCIBLE - $FTBFS - $DISABLED || true)
RELEVANTLOGGED=$(cat lists/unique-downstream.txt 2>/dev/null | sed 's,\(.*\)=\(.*\)$,logs/\1/\2/buildlog.txt,' | xargs --no-run-if-empty ls 2> /dev/null | wc -l)
RELEVANTREPRODUCIBLE=$(cat lists/unique-downstream.txt 2>/dev/null | sed 's,\(.*\)=\(.*\)$,logs/\1/\2/reproducible.txt,' | xargs --no-run-if-empty ls 2> /dev/null | wc -l)
PERCENT_BUILT=$(echo "100 * ($RELEVANTLOGGED / $TOTAL)" | bc -l 2>/dev/null | cut -d. -f1)
test -z "$PERCENT_BUILT" && PERCENT_BUILT=0
PERCENT_REPRODUCIBLE=$(echo "100 * ($RELEVANTREPRODUCIBLE / $TOTAL)" | bc -l 2>/dev/null | cut -d. -f1)
test -z "$PERCENT_REPRODUCIBLE" && PERCENT_REPRODUCIBLE=0
PERCENT_SOFAR=$(echo "100 * ($RELEVANTREPRODUCIBLE / $RELEVANTLOGGED)" | bc -l 2>/dev/null | cut -d. -f1)
test -z "$PERCENT_SOFAR" && PERCENT_SOFAR=0
CURRENT=$(cat lists/timestamp-* 2>/dev/null|xargs -I{} date --date="{}" +%s|sort|tail -1)
test -z "$CURRENT" && CURRENT=0
CURRENT_STRING=$(date -u --date="@$CURRENT")

cat<<EOF

We have reproducibly built **$PERCENT_REPRODUCIBLE%** of the
difference between **DEBDISTREPRODUCE_DOWNSTREAM_NAME** and
**DEBDISTREPRODUCE_UPSTREAM_NAME**!  That is **$PERCENT_SOFAR%** of
the packages we have built, and we have built **$PERCENT_BUILT%** or
**$RELEVANTLOGGED** of the **$TOTAL** source packages to rebuild.

DEBDISTREPRODUCE_DOWNSTREAM_NAME (on amd64) contains binary packages
that were added/modified compared to what is in
DEBDISTREPRODUCE_UPSTREAM_NAME (on amd64) that corresponds to
**$UNIQUEPKGS** source packages.  Some binary packages exists in more
than one version, so there is a total of **$TOTAL** source packages to
rebuild.  Of these we have built **$RELEVANTREPRODUCIBLE**
reproducibly out of the **$RELEVANTLOGGED** builds so far.

We have build logs for **$LOGGED** builds, which may exceed the number
of total source packages to rebuild when a particular source package
(or source package version) has been removed from the archive.  Of the
packages we built, **$REPRODUCIBLE** packages are reproducible and
there are **$UNREPRODUCIBLE** packages that we could not reproduce.
Building **$FTBFS** package had build failures.  We do not attempt to
build **$DISABLED** packages.

[[_TOC_]]
EOF

if test "$UNREPRODUCIBLE" -gt 0; then
    cat<<EOF

### Unreproducible packages

The following **$UNREPRODUCIBLE** packages unfortunately differ in
some way compared to the version distributed in the archive.  Please
investigate the build log and [diffoscope](https://diffoscope.org)
output and help us fix it!

| Package | Version | Diffoscope output | Link to build log |
| ------- | ------- | ----------------- | ----------------- |
EOF

    for f in $(ls logs/*/*/buildlog.txt | sort); do
	pkg=$(echo $f | sed 's,logs/\([^/]*\)/.*,\1,')
	ver=$(echo $f | sed 's,logs/[^/]*/\([^/]*\)/.*,\1,')
	timestamp=$(head -2 $f | tail -1)
	test -f logs/$pkg/$ver/ftbfs.txt && continue
	test -f logs/$pkg/$ver/reproducible.txt && continue
	if test -f diffoscope/$pkg/$ver/index.html; then
	    echo -n "| $pkg | $ver | [diffoscope output]($PAGESURL/diffoscope/$pkg/$ver/index.html) "
	else
	    echo -n "| $pkg | $ver | no diffoscope output (probably too large) "
	fi
	echo "| [build log from $timestamp]($PROJECTURL/-/blob/main/logs/$pkg/$ver/buildlog.txt) |"
    done
fi

if test "$FTBFS" -gt 0; then
    cat<<EOF

### Build failures

The following **$FTBFS** packages have build failures, making it
impossible to even compare the binary package in the archive with what
we are able to build locally.

#### Missing source code

The archive is missing source code for the following source packages,
for which the archive is shipping a binary packages that claims it was
built using that particular source package/version.

| Package | Version | Link to build log |
| ------- | ------- | ----------------- |
EOF

    for f in logs/*/*/ftbfs.txt; do
	pkg=$(echo $f | sed 's,logs/\([^/]*\)/.*,\1,')
	ver=$(echo $f | sed 's,logs/[^/]*/\([^/]*\)/.*,\1,')
	buildlog=$(echo $f | sed 's,/ftbfs.txt,/buildlog.txt,')
	(tail -n1 $buildlog | grep -q '^E: Unable to find a source package for' && grep -q "^$pkg=$ver\$" lists/unique-downstream.txt) || continue
	timestamp=$(head -2 $buildlog | tail -1)
	echo "| $pkg | $ver | [build log from $timestamp]($PROJECTURL/-/blob/main/logs/$pkg/$ver/buildlog.txt) |"
    done    

    cat<<EOF

#### Other build failures

Please investigate the build log and help us fix it!

| Package | Version | Link to build log |
| ------- | ------- | ----------------- |
EOF

    for f in logs/*/*/ftbfs.txt; do
	pkg=$(echo $f | sed 's,logs/\([^/]*\)/.*,\1,')
	ver=$(echo $f | sed 's,logs/[^/]*/\([^/]*\)/.*,\1,')
	buildlog=$(echo $f | sed 's,/ftbfs.txt,/buildlog.txt,')
	(tail -n1 $buildlog | grep -q '^E: Unable to find a source package for' && grep -q "^$pkg=$ver\$" lists/unique-downstream.txt) && continue
	timestamp=$(head -2 $buildlog | tail -1)
	echo "| $pkg | $ver | [build log from $timestamp]($PROJECTURL/-/blob/main/logs/$pkg/$ver/buildlog.txt) |"
    done    
fi

if test "$REPRODUCIBLE" -gt 0; then
    cat<<EOF

### Reproducible packages

The following **$REPRODUCIBLE** packages can be built locally to
produce the exact same package that is shipped in the archive.

| Package | Version | Link to build log |
| ------- | ------- | ----------------- |
EOF

    for f in logs/*/*/reproducible.txt; do
	pkg=$(echo $f | sed 's,logs/\([^/]*\)/.*,\1,')
	ver=$(echo $f | sed 's,logs/[^/]*/\([^/]*\)/.*,\1,')
	buildlog=$(echo $f | sed 's,/reproducible.txt,/buildlog.txt,')
	timestamp=$(head -2 $buildlog | tail -1)
	echo "| $pkg | $ver | [build log from $timestamp]($PROJECTURL/-/blob/main/logs/$pkg/$ver/buildlog.txt) |"
    done
fi

if test "$DISABLED" -gt 0; then
    cat<<EOF

### Manually disabled packages

Currently the following **$DISABLED** packages are being held back
from building manually.  Usually the reason is high computational
resoure demands; see
[lists/disabled-downstream.txt](lists/disabled-downstream.txt).

| Package | Version |
| ------- | ------- |
EOF

    for pkg in $(grep -v -e '^$' -e '^#' lists/disabled-downstream.txt | uniq | sort); do
	echo "| $pkg | $ver |"
    done
fi

if test "$REST" -gt 0; then
    cat<<EOF

### Packages that remains to be built

The following **$REST** packages have not yet been built.

| Package | Version |
| ------- | ------- |
EOF

    for pkgver in $(cat lists/unique-downstream.txt); do
	pkg=$(echo $pkgver | sed -n 's,=.*,,p')
	ver=$(echo $pkgver | sed -n 's,.*=,,p')
	test -f logs/$pkg/$ver/buildlog.txt && continue
	grep -q "^$pkg\$" lists/disabled-downstream.txt 2>/dev/null && continue
	echo "| $pkg | $ver |"
    done
fi

if test "$CURRENT" -gt 0; then
    cat<<EOF

### Timestamps

The timestamps of the archives used as input to find out which
packages to reproduce are as follows.  To be precise, these are the
\`Date\` field in the respectively \`Release\` file used to construct
the list of packages to evaluate.

When speaking about "current" status of this effort it makes sense to
use the latest timestamp from the set below, which is
**$CURRENT_STRING**.

| Suite | DEBDISTREPRODUCE_UPSTREAM_NAME | DEBDISTREPRODUCE_DOWNSTREAM_NAME |
| ----- | ------------------------------ | -------------------------------- |
EOF

    for mapping in $MAPPINGS; do
	up=$(echo $mapping | sed -n 's,@.*,,p' | sed 's,:.*,,')
	down=$(echo $mapping | sed -n 's,.*@,,p' | sed 's,:.*,,')
	echo "| $up / $down | $(cat lists/timestamp-$up.txt) | $(cat lists/timestamp-$down.txt) |"
    done
fi

exit 0
