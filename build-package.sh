#!/bin/sh

# Copyright (C) 2023 Simon Josefsson <simon@josefsson.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# podman run -it --rm kpengboy/trisquel:11.0
# cat /etc/apt/sources.list | sed s/deb/deb-src/ >> /etc/apt/sources.list.d/src.list
# apt-get update
# apt-get install -qq --no-install-recommends -y build-essential pkgbinarymangler eatmydata wget | tail
# apt-get install -qq -y devscripts diffoscope | tail

# podman run -it --rm registry.gitlab.com/debdistutils/reproduce-trisquel:trisquel-aramo
# apt-get update

set -x
set -e

pkg=$1
ver=$2

test -n "$pkg"
test -n "$ver"

url=$(apt-get --print-uris --yes source $pkg=$ver | tail -1 | cut -d\' -f2 | sed 's,\(.*\)/[^/]*,\1,')

builddir=/build/$pkg
if test "$pkg" = "initramfs-tools"; then
    # XXX FIXME
    # How to get *.buildinfo from trisquel builds automatically?
    # We found a (filtered..) build log here for this package manually
    # https://jenkins.trisquel.org/job/binary/label=amd64/6816/console
    builddir=/build/initramfs-tools-bzRLUp
elif test "$pkg" = "wget"; then
    builddir=/build/wget-qGED2v
elif test "$pkg" = "btrfs-progs"; then
    builddir=/build/btrfs-progs-3hWfCn
elif test "$pkg" = "openssl"; then
    builddir=/build/openssl-mzafz0
elif test "$pkg" = "openssh"; then
    builddir=/build/openssh-v1ondY
fi

notrimdch=
if test -f /etc/devuan_version; then
    builddir=/build
    notrimdch=" notrimdch"
fi

nocheck=""
case $pkg in
    acdcli | epiphany-browser | cryptsetup | devscripts | emacs | firefox | glibc | grub2-unsigned | grub2 | libreoffice | network-manager | nodejs | rsyslog | rustc | systemd | thunderbird)
	nocheck=" nocheck";;
esac

parallel=""
case $pkg in
    libreoffice)
	# by default it seems libreoffice uses only a -j4 setting
	parallel=" parallel=24";;
esac

mkdir -p $builddir
THEN=$(pwd)
cd $builddir
{
    date && \
	id && \
	apt-get source --only-source $pkg=$ver && \
	env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source $pkg=$ver && \
	cd `find . -maxdepth 1 -name "$pkg*" -type d` && \
	eatmydata env DEB_BUILD_OPTIONS="noautodbgsym$notrimdch$nocheck$parallel" dpkg-buildpackage --no-sign && \
	date && \
	cd .. && \
	ls -la && \
	sha256sum $(find . -maxdepth 1 -type f) && \
	mkdir published && \
	cd published && \
	for f in $(cd ../ && ls *.deb *.udeb); do \
	    wget --retry-connrefused --retry-on-host-error --tries=3 "$url/$f"; \
	done && \
	sha256sum $(find . -maxdepth 1 -type f) | tee ../SHA256SUMS && \
	cd .. && \
	sha256sum -c SHA256SUMS && \
	echo Package $pkg version $ver is reproducible!
} 2>&1 | tee --output-error=warn buildlog.txt | head -500
cd $THEN

mkdir -p logs/$pkg/$ver
cp $builddir/buildlog.txt logs/$pkg/$ver/

if ! test -f $builddir/$pkg*.buildinfo; then
    touch logs/$pkg/$ver/ftbfs.txt
    exit 0
fi

if tail -1 $builddir/buildlog.txt | grep 'is reproducible!'; then
    touch logs/$pkg/$ver/reproducible.txt
fi

mkdir rebuilt
for deb in $(cd $builddir && ls *.deb *.udeb); do
    cp $builddir/$deb rebuilt
done

mv $builddir/published .

for deb in $(cd rebuilt && ls *.deb *.udeb); do
    debdiff published/$deb rebuilt/$deb \
	|| true # XXX fail on debdiff exit code 255?
done

dir=diffoscope/$pkg/$ver
mkdir -p $dir
timeout "30m" diffoscope --max-report-size 101943040 --output-empty --exclude-directory-metadata=yes --html-dir $dir published rebuilt \
    || true # XXX fail on diffoscope exit code 2?

exit 0
