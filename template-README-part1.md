# Reproducible builds of DEBDISTREPRODUCE_DOWNSTREAM_NAME

This project provides [reproducible
build](https://reproducible-builds.org/) status for
[DEBDISTREPRODUCE_DOWNSTREAM_NAME](DEBDISTREPRODUCE_DOWNSTREAM_URL) on
DEBDISTREPRODUCE_ARCH.

## Status
