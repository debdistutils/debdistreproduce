#!/bin/sh

# Copyright (C) 2023 Simon Josefsson <simon@josefsson.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e
set -x

LANG=C
LC_ALL=C

DEBDISTDIFF=${DEBDISTDIFF:-debdistdiff}

UPSTREAM="$1"
DOWNSTREAM="$2"
MAPPINGS="$3"
ARCH="$4"

test -d "$UPSTREAM"
test -d "$DOWNSTREAM"
test -n "$MAPPINGS"

mkdir -p lists

rm -f lists/modified-*.txt lists/added-*.txt lists/unique-*.txt lists/timestamp-*.txt

for mapping in $MAPPINGS; do
    : $mapping
    up=$(echo $mapping | sed -n 's,@.*,,p')
    down=$(echo $mapping | sed -n 's,.*@,,p')
    ARGS="--modified-sources --arch $ARCH --dist $UPSTREAM --dist $DOWNSTREAM"
    $DEBDISTDIFF $ARGS $up $down > lists/modified-part-$up--$down.txt
    ARGS="--added-sources --arch $ARCH --dist $UPSTREAM --dist $DOWNSTREAM"
    $DEBDISTDIFF $ARGS $up $down > lists/added-part-$up--$down.txt
done

cat lists/modified-part*.txt | sort | uniq > lists/modified-downstream.txt

cat lists/added-part*.txt | sort | uniq > lists/added-downstream.txt

cat lists/added-downstream.txt lists/modified-downstream.txt | sort | uniq > lists/unique-downstream.txt

for mapping in $MAPPINGS; do
    : $mapping
    up=$(echo $mapping | sed -n 's,@.*,,p' | sed 's,:.*,,')
    grep ^Date: $UPSTREAM/dists/$up/Release | sed 's,Date: ,,' > lists/timestamp-$up.txt
done

for mapping in $MAPPINGS; do
    : $mapping
    down=$(echo $mapping | sed -n 's,.*@,,p' | sed 's,:.*,,')
    grep ^Date: $DOWNSTREAM/dists/$down/Release | sed 's,Date: ,,' > lists/timestamp-$down.txt
done

exit 0
